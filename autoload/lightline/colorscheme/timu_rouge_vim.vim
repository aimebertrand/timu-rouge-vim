" Project: Rouge Theme for Vim
" Vim port of the Rouge Theme for VSCode - https://github.com/josefaidt/rouge-theme
" Code boilerplate from nord-vim
" License: MIT

let s:timu_rouge_vim_version="1.0.0"
let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

let s:sg0 = ["#1F2A3F", "NONE"]
let s:sg1 = ["#A7ACB9", 0]
let s:sg2 = ["#5D636E", "NONE"]
let s:sg3 = ["#64727d", 8]
let s:sg4 = ["#E8E9EB", "NONE"]
let s:sg5 = ["#F0F4FC", 7]
let s:sg6 = ["#FAFFF6", 15]
let s:sg7 = ["#88C0D0", 14]
let s:sg8 = ["#507681", 6]
let s:sg9 = ["#6e94b9", 4]
let s:sg10 = ["#1E6378", 12]
let s:sg11 = ["#c6797e", 1]
let s:sg12 = ["#eabe9a", 11]
let s:sg13 = ["#F7E3AF", 3]
let s:sg14 = ["#A3B09A", 2]
let s:sg15 = ["#b18bb1", 5]
let s:sg16 = ["#DB6E8F", 10]

let s:p.normal.left = [ [ s:sg0, s:sg10 ], [ s:sg0, s:sg1 ] ]
let s:p.normal.middle = [ [ s:sg0, s:sg3 ] ]
let s:p.normal.right = [ [ s:sg0, s:sg3 ], [ s:sg0, s:sg3 ] ]
let s:p.normal.warning = [ [ s:sg0, s:sg13 ] ]
let s:p.normal.error = [ [ s:sg0, s:sg11 ] ]

let s:p.inactive.left =  [ [ s:sg1, s:sg3 ], [ s:sg1, s:sg3 ] ]
let s:p.inactive.middle = g:sg_uniform_status_lines == 0 ? [ [ s:sg1, s:sg3 ] ] : [ [ s:sg1, s:sg3 ] ]
let s:p.inactive.right = [ [ s:sg1, s:sg3 ], [ s:sg1, s:sg3 ] ]

let s:p.insert.left = [ [ s:sg0, s:sg11 ], [ s:sg0, s:sg1 ] ]
let s:p.replace.left = [ [ s:sg0, s:sg15 ], [ s:sg0, s:sg1 ] ]
let s:p.visual.left = [ [ s:sg0, s:sg13 ], [ s:sg0, s:sg1 ] ]

let s:p.tabline.left = [ [ s:sg5, s:sg3 ] ]
let s:p.tabline.middle = [ [ s:sg5, s:sg3 ] ]
let s:p.tabline.right = [ [ s:sg5, s:sg3 ] ]
let s:p.tabline.tabsel = [ [ s:sg1, s:sg8 ] ]

let g:lightline#colorscheme#timu_rouge_vim#palette = lightline#colorscheme#flatten(s:p)
