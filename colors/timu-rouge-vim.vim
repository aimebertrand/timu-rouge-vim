" Project: Rouge Theme for Vim
" Vim port of the Rouge Theme for VSCode - https://github.com/josefaidt/rouge-theme
" Code boilerplate from nord-vim
" License: MIT

if version > 580
  hi clear
  if exists("syntax_on")
    syntax reset
  endif
endif

let g:colors_name = "timu-rouge-vim"
let s:timu_rouge_vim_version="1.0.0"
set background=dark

let s:sg0_gui = "#172030"
let s:sg1_gui = "#A7ACB9"
let s:sg2_gui = "#5D636E"
let s:sg3_gui = "#64727d"
let s:sg3_gui_bright = "#A7ACB9"
let s:sg4_gui = "#E8E9EB"
let s:sg5_gui = "#F0F4FC"
let s:sg6_gui = "#FAFFF6"
let s:sg7_gui = "#88C0D0"
let s:sg8_gui = "#507681"
let s:sg9_gui = "#6e94b9"
let s:sg10_gui = "#1E6378"
let s:sg11_gui = "#c6797e"
let s:sg11_gui_dark = "#DB6E8F"
let s:sg12_gui = "#eabe9a"
let s:sg13_gui = "#F7E3AF"
let s:sg14_gui = "#A3B09A"
let s:sg15_gui = "#b18bb1"
let s:sg16_gui = "#1F2A3F"

let s:sg1_term = "0"
let s:sg3_term = "8"
let s:sg5_term = "7"
let s:sg6_term = "15"
let s:sg7_term = "14"
let s:sg8_term = "6"
let s:sg9_term = "4"
let s:sg10_term = "12"
let s:sg11_term = "1"
let s:sg12_term = "11"
let s:sg13_term = "3"
let s:sg14_term = "2"
let s:sg15_term = "5"

let s:sg3_gui_brightened = [
  \ s:sg3_gui,
  \ "#4e586d",
  \ "#505b70",
  \ "#525d73",
  \ "#556076",
  \ "#576279",
  \ "#59647c",
  \ "#5b677f",
  \ "#5d6982",
  \ "#5f6c85",
  \ "#616e88",
  \ "#63718b",
  \ "#66738e",
  \ "#687591",
  \ "#6a7894",
  \ "#6d7a96",
  \ "#6f7d98",
  \ "#72809a",
  \ "#75829c",
  \ "#78859e",
  \ "#7b88a1",
\ ]

if !exists("g:sg_bold")
  let g:sg_bold = 1
endif

let s:bold = "bold,"
if g:sg_bold == 0
  let s:bold = ""
endif

if !exists("g:sg_italic")
  if has("gui_running") || $TERM_ITALICS == "true"
    let g:sg_italic = 1
  else
    let g:sg_italic = 0
  endif
endif

let s:italic = "italic,"
if g:sg_italic == 0
  let s:italic = ""
endif

let s:underline = "underline,"
if ! get(g:, "sg_underline", 1)
  let s:underline = "NONE,"
endif

let s:italicize_comments = ""
if exists("g:sg_italic_comments")
  if g:sg_italic_comments == 1
    let s:italicize_comments = s:italic
  endif
endif

if !exists('g:sg_uniform_status_lines')
  let g:sg_uniform_status_lines = 0
endif

function! s:logWarning(msg)
  echohl WarningMsg
  echomsg 'rouge: warning: ' . a:msg
  echohl None
endfunction

if !exists("g:sg_uniform_diff_background")
  let g:sg_uniform_diff_background = 0
endif

if !exists("g:sg_cursor_line_number_background")
  let g:sg_cursor_line_number_background = 0
endif

if !exists("g:sg_bold_vertical_split_line")
  let g:sg_bold_vertical_split_line = 0
endif

function! s:hi(group, guifg, guibg, ctermfg, ctermbg, attr, guisp)
  if a:guifg != ""
    exec "hi " . a:group . " guifg=" . a:guifg
  endif
  if a:guibg != ""
    exec "hi " . a:group . " guibg=" . a:guibg
  endif
  if a:ctermfg != ""
    exec "hi " . a:group . " ctermfg=" . a:ctermfg
  endif
  if a:ctermbg != ""
    exec "hi " . a:group . " ctermbg=" . a:ctermbg
  endif
  if a:attr != ""
    exec "hi " . a:group . " gui=" . a:attr . " cterm=" . substitute(a:attr, "undercurl", s:underline, "")
  endif
  if a:guisp != ""
    exec "hi " . a:group . " guisp=" . a:guisp
  endif
endfunction

"+---------------+
"+ UI Components +
"+---------------+
"+--- Attributes ---+
call s:hi("Bold", "", "", "", "", s:bold, "")
call s:hi("Italic", "", "", "", "", s:italic, "")
call s:hi("Underline", "", "", "", "", s:underline, "")

"+--- Editor ---+
call s:hi("ColorColumn", "", s:sg1_gui, "NONE", s:sg1_term, "", "")
call s:hi("Cursor", s:sg11_gui_dark, s:sg12_gui, "", "NONE", "", "")
call s:hi("CursorLine", "", s:sg16_gui, "NONE", s:sg1_term, "NONE", "")
call s:hi("Error", s:sg4_gui, s:sg11_gui, "", s:sg11_term, "", "")
call s:hi("iCursor", s:sg0_gui, s:sg4_gui, "", "NONE", "", "")
call s:hi("LineNr", s:sg3_gui, "NONE", s:sg3_term, "NONE", "", "")
call s:hi("MatchParen", s:sg8_gui, s:sg3_gui, s:sg8_term, s:sg3_term, "", "")
call s:hi("NonText", s:sg2_gui, "", s:sg3_term, "", "", "")
call s:hi("Normal", s:sg4_gui, s:sg0_gui, "NONE", "NONE", "", "")
call s:hi("PMenu", s:sg4_gui, s:sg16_gui, "NONE", s:sg1_term, "NONE", "")
call s:hi("PmenuSbar", s:sg4_gui, s:sg2_gui, "NONE", s:sg1_term, "", "")
call s:hi("PMenuSel", s:sg11_gui_dark, s:sg2_gui, s:sg8_term, s:sg3_term, "", "")
call s:hi("PmenuThumb", s:sg8_gui, s:sg3_gui, "NONE", s:sg3_term, "", "")
call s:hi("SpecialKey", s:sg3_gui, "", s:sg3_term, "", "", "")
call s:hi("SpellBad", s:sg11_gui, s:sg0_gui, s:sg11_term, "NONE", "undercurl", s:sg11_gui)
call s:hi("SpellCap", s:sg13_gui, s:sg0_gui, s:sg13_term, "NONE", "undercurl", s:sg13_gui)
call s:hi("SpellLocal", s:sg5_gui, s:sg0_gui, s:sg5_term, "NONE", "undercurl", s:sg5_gui)
call s:hi("SpellRare", s:sg6_gui, s:sg0_gui, s:sg6_term, "NONE", "undercurl", s:sg6_gui)
call s:hi("Visual", "", s:sg2_gui, "", s:sg1_term, "", "")
call s:hi("VisualNOS", "", s:sg2_gui, "", s:sg1_term, "", "")
"+- Neovim Support -+
call s:hi("healthError", s:sg11_gui, s:sg1_gui, s:sg11_term, s:sg1_term, "", "")
call s:hi("healthSuccess", s:sg14_gui, s:sg1_gui, s:sg14_term, s:sg1_term, "", "")
call s:hi("healthWarning", s:sg13_gui, s:sg1_gui, s:sg13_term, s:sg1_term, "", "")
call s:hi("TermCursorNC", "", s:sg1_gui, "", s:sg1_term, "", "")

"+- Vim 8 Terminal Colors -+
if has('terminal')
  let g:terminal_ansi_colors = [s:sg1_gui, s:sg11_gui, s:sg14_gui, s:sg13_gui, s:sg9_gui, s:sg15_gui, s:sg8_gui, s:sg5_gui, s:sg3_gui, s:sg11_gui, s:sg14_gui, s:sg13_gui, s:sg9_gui, s:sg15_gui, s:sg7_gui, s:sg6_gui]
endif

"+- Neovim Terminal Colors -+
if has('nvim')
  let g:terminal_color_0 = s:sg1_gui
  let g:terminal_color_1 = s:sg11_gui
  let g:terminal_color_2 = s:sg14_gui
  let g:terminal_color_3 = s:sg13_gui
  let g:terminal_color_4 = s:sg9_gui
  let g:terminal_color_5 = s:sg15_gui
  let g:terminal_color_6 = s:sg8_gui
  let g:terminal_color_7 = s:sg5_gui
  let g:terminal_color_8 = s:sg3_gui
  let g:terminal_color_9 = s:sg11_gui
  let g:terminal_color_10 = s:sg14_gui
  let g:terminal_color_11 = s:sg13_gui
  let g:terminal_color_12 = s:sg9_gui
  let g:terminal_color_13 = s:sg15_gui
  let g:terminal_color_14 = s:sg7_gui
  let g:terminal_color_15 = s:sg6_gui
endif

"+--- Gutter ---+
call s:hi("CursorColumn", "", s:sg1_gui, "NONE", s:sg1_term, "", "")
if g:sg_cursor_line_number_background == 0
  call s:hi("CursorLineNr", s:sg4_gui, "", "NONE", "", "NONE", "")
else
  call s:hi("CursorLineNr", s:sg4_gui, s:sg1_gui, "NONE", s:sg1_term, "NONE", "")
endif
call s:hi("Folded", s:sg3_gui, s:sg1_gui, s:sg3_term, s:sg1_term, s:bold, "")
call s:hi("FoldColumn", s:sg3_gui, s:sg0_gui, s:sg3_term, "NONE", "", "")
call s:hi("SignColumn", s:sg1_gui, s:sg0_gui, s:sg1_term, "NONE", "", "")

"+--- Navigation ---+
call s:hi("Directory", s:sg8_gui, "", s:sg8_term, "NONE", "", "")

"+--- Prompt/Status ---+
call s:hi("EndOfBuffer", s:sg1_gui, "", s:sg1_term, "NONE", "", "")
call s:hi("ErrorMsg", s:sg4_gui, s:sg11_gui, "NONE", s:sg11_term, "", "")
call s:hi("ModeMsg", s:sg4_gui, "", "", "", "", "")
call s:hi("MoreMsg", s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("Question", s:sg4_gui, "", "NONE", "", "", "")
if g:sg_uniform_status_lines == 0
  call s:hi("StatusLine", s:sg8_gui, s:sg3_gui, s:sg8_term, s:sg3_term, "NONE", "")
  call s:hi("StatusLineNC", s:sg4_gui, s:sg1_gui, "NONE", s:sg1_term, "NONE", "")
  call s:hi("StatusLineTerm", s:sg8_gui, s:sg3_gui, s:sg8_term, s:sg3_term, "NONE", "")
  call s:hi("StatusLineTermNC", s:sg4_gui, s:sg1_gui, "NONE", s:sg1_term, "NONE", "")
else
  call s:hi("StatusLine", s:sg8_gui, s:sg3_gui, s:sg8_term, s:sg3_term, "NONE", "")
  call s:hi("StatusLineNC", s:sg4_gui, s:sg3_gui, "NONE", s:sg3_term, "NONE", "")
  call s:hi("StatusLineTerm", s:sg8_gui, s:sg3_gui, s:sg8_term, s:sg3_term, "NONE", "")
  call s:hi("StatusLineTermNC", s:sg4_gui, s:sg3_gui, "NONE", s:sg3_term, "NONE", "")
endif
call s:hi("WarningMsg", s:sg0_gui, s:sg13_gui, s:sg1_term, s:sg13_term, "", "")
call s:hi("WildMenu", s:sg8_gui, s:sg1_gui, s:sg8_term, s:sg1_term, "", "")

"+--- Search ---+
call s:hi("IncSearch", s:sg6_gui, s:sg10_gui, s:sg6_term, s:sg10_term, s:underline, "")
call s:hi("Search", s:sg1_gui, s:sg8_gui, s:sg1_term, s:sg8_term, "NONE", "")

"+--- Tabs ---+
call s:hi("TabLine", s:sg4_gui, s:sg1_gui, "NONE", s:sg1_term, "NONE", "")
call s:hi("TabLineFill", s:sg4_gui, s:sg1_gui, "NONE", s:sg1_term, "NONE", "")
call s:hi("TabLineSel", s:sg8_gui, s:sg3_gui, s:sg8_term, s:sg3_term, "NONE", "")

"+--- Window ---+
call s:hi("Title", s:sg4_gui, "", "NONE", "", "NONE", "")

if g:sg_bold_vertical_split_line == 0
  call s:hi("VertSplit", s:sg2_gui, s:sg0_gui, s:sg3_term, "NONE", "NONE", "")
else
  call s:hi("VertSplit", s:sg2_gui, s:sg1_gui, s:sg3_term, s:sg1_term, "NONE", "")
endif

"+----------------------+
"+ Language Base Groups +
"+----------------------+
call s:hi("Boolean", s:sg11_gui_dark, "", s:sg12_term, "", "", "")
call s:hi("Character", s:sg14_gui, "", s:sg14_term, "", "", "")
call s:hi("Comment", s:sg3_gui_bright, "", s:sg3_term, "", s:italicize_comments, "")
call s:hi("Conditional", s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("Constant", s:sg11_gui, "", "NONE", "", "", "")
call s:hi("Define", s:sg12_gui, "", s:sg12_term, "", "", "")
call s:hi("Delimiter", s:sg6_gui, "", s:sg6_term, "", "", "")
call s:hi("Exception", s:sg9_gui, "", s:sg9_term, "", "", "")
call s:hi("Float", s:sg15_gui, "", s:sg15_term, "", "", "")
call s:hi("Function", s:sg15_gui, "", s:sg15_term, "", "", "")
call s:hi("Identifier", s:sg11_gui_dark, "", "NONE", "", "NONE", "")
call s:hi("Include", s:sg11_gui, "", s:sg11_term, "", "", "")
call s:hi("Keyword", s:sg15_gui, "", s:sg15_term, "", "", "")
call s:hi("Label", s:sg11_gui, "", "NONE", "", "", "")
call s:hi("Number", s:sg15_gui, "", s:sg15_term, "", "", "")
call s:hi("Operator", s:sg8_gui, "", s:sg8_term, "", "NONE", "")
call s:hi("PreProc", s:sg12_gui, "", s:sg12_term, "", "NONE", "")
call s:hi("Repeat", s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("Special", s:sg11_gui, "", "NONE", "", "", "")
call s:hi("SpecialChar", s:sg13_gui, "", s:sg13_term, "", "", "")
call s:hi("SpecialComment", s:sg3_gui, "", s:sg3_term, "", s:italicize_comments, "")
call s:hi("Statement", s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("StorageClass", s:sg9_gui, "", s:sg9_term, "", "", "")
call s:hi("String", s:sg14_gui, "", s:sg14_term, "", "", "")
call s:hi("Structure", s:sg12_gui, "", s:sg12_term, "", "", "")
call s:hi("Tag", s:sg11_gui, "", s:sg11_term, "", "", "")
call s:hi("Todo", s:sg13_gui, "NONE", s:sg13_term, "NONE", "", "")
call s:hi("Type", s:sg13_gui, "", s:sg13_term, "", "NONE", "")
call s:hi("Typedef", s:sg9_gui, "", s:sg9_term, "", "", "")
hi! link Macro Define
hi! link PreCondit PreProc

"+-----------+
"+ Languages +
"+-----------+
call s:hi("asciidocAttributeEntry", s:sg10_gui, "", s:sg10_term, "", "", "")
call s:hi("asciidocAttributeList", s:sg10_gui, "", s:sg10_term, "", "", "")
call s:hi("asciidocAttributeRef", s:sg10_gui, "", s:sg10_term, "", "", "")
call s:hi("asciidocHLabel", s:sg9_gui, "", s:sg9_term, "", "", "")
call s:hi("asciidocListingBlock", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("asciidocMacroAttributes", s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("asciidocOneLineTitle", s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("asciidocPassthroughBlock", s:sg9_gui, "", s:sg9_term, "", "", "")
call s:hi("asciidocQuotedMonospaced", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("asciidocTriplePlusPassthrough", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link asciidocAdmonition Keyword
hi! link asciidocAttributeRef markdownH1
hi! link asciidocBackslash Keyword
hi! link asciidocMacro Keyword
hi! link asciidocQuotedBold Bold
hi! link asciidocQuotedEmphasized Italic
hi! link asciidocQuotedMonospaced2 asciidocQuotedMonospaced
hi! link asciidocQuotedUnconstrainedBold asciidocQuotedBold
hi! link asciidocQuotedUnconstrainedEmphasized asciidocQuotedEmphasized
hi! link asciidocURL markdownLinkText

call s:hi("awkCharClass", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("awkPatterns", s:sg9_gui, "", s:sg9_term, "", s:bold, "")
hi! link awkArrayElement Identifier
hi! link awkBoolLogic Keyword
hi! link awkBrktRegExp SpecialChar
hi! link awkComma Delimiter
hi! link awkExpression Keyword
hi! link awkFieldVars Identifier
hi! link awkLineSkip Keyword
hi! link awkOperator Operator
hi! link awkRegExp SpecialChar
hi! link awkSearch Keyword
hi! link awkSemicolon Delimiter
hi! link awkSpecialCharacter SpecialChar
hi! link awkSpecialPrintf SpecialChar
hi! link awkVariables Identifier

call s:hi("cIncluded", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link cOperator Operator
hi! link cPreCondit PreCondit

call s:hi("cmakeGeneratorExpression", s:sg10_gui, "", s:sg10_term, "", "", "")

hi! link csPreCondit PreCondit
hi! link csType Type
hi! link csXmlTag SpecialComment

call s:hi("cssAttributeSelector", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("cssDefinition", s:sg7_gui, "", s:sg7_term, "", "NONE", "")
call s:hi("cssIdentifier", s:sg7_gui, "", s:sg7_term, "", s:underline, "")
call s:hi("cssStringQ", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link cssAttr Keyword
hi! link cssBraces Delimiter
hi! link cssClassName cssDefinition
hi! link cssColor Number
hi! link cssProp cssDefinition
hi! link cssPseudoClass cssDefinition
hi! link cssPseudoClassId cssPseudoClass
hi! link cssVendor Keyword

call s:hi("dosiniHeader", s:sg8_gui, "", s:sg8_term, "", "", "")
hi! link dosiniLabel Type

call s:hi("dtBooleanKey", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("dtExecKey", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("dtLocaleKey", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("dtNumericKey", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("dtTypeKey", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link dtDelim Delimiter
hi! link dtLocaleValue Keyword
hi! link dtTypeValue Keyword

if g:sg_uniform_diff_background == 0
  call s:hi("DiffAdd", s:sg14_gui, s:sg0_gui, s:sg14_term, "NONE", "inverse", "")
  call s:hi("DiffChange", s:sg13_gui, s:sg0_gui, s:sg13_term, "NONE", "inverse", "")
  call s:hi("DiffDelete", s:sg11_gui, s:sg0_gui, s:sg11_term, "NONE", "inverse", "")
  call s:hi("DiffText", s:sg9_gui, s:sg0_gui, s:sg9_term, "NONE", "inverse", "")
else
  call s:hi("DiffAdd", s:sg14_gui, s:sg1_gui, s:sg14_term, s:sg1_term, "", "")
  call s:hi("DiffChange", s:sg13_gui, s:sg1_gui, s:sg13_term, s:sg1_term, "", "")
  call s:hi("DiffDelete", s:sg11_gui, s:sg1_gui, s:sg11_term, s:sg1_term, "", "")
  call s:hi("DiffText", s:sg9_gui, s:sg1_gui, s:sg9_term, s:sg1_term, "", "")
endif
" Legacy groups for official git.vim and diff.vim syntax
hi! link diffAdded DiffAdd
hi! link diffChanged DiffChange
hi! link diffRemoved DiffDelete

call s:hi("gitconfigVariable", s:sg7_gui, "", s:sg7_term, "", "", "")

call s:hi("goBuiltins", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link goConstants Keyword

call s:hi("helpBar", s:sg3_gui, "", s:sg3_term, "", "", "")
call s:hi("helpHyperTextJump", s:sg8_gui, "", s:sg8_term, "", s:underline, "")

call s:hi("htmlArg", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("htmlLink", s:sg4_gui, "", "", "", "NONE", "NONE")
hi! link htmlBold Bold
hi! link htmlEndTag htmlTag
hi! link htmlItalic Italic
hi! link htmlH1 markdownH1
hi! link htmlH2 markdownH1
hi! link htmlH3 markdownH1
hi! link htmlH4 markdownH1
hi! link htmlH5 markdownH1
hi! link htmlH6 markdownH1
hi! link htmlSpecialChar SpecialChar
hi! link htmlTag Keyword
hi! link htmlTagN htmlTag

call s:hi("javaDocTags", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link javaCommentTitle Comment
hi! link javaScriptBraces Delimiter
hi! link javaScriptIdentifier Keyword
hi! link javaScriptNumber Number

call s:hi("jsonKeyword", s:sg7_gui, "", s:sg7_term, "", "", "")

call s:hi("lessClass", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link lessAmpersand Keyword
hi! link lessCssAttribute Delimiter
hi! link lessFunction Function
hi! link cssSelectorOp Keyword

hi! link lispAtomBarSymbol SpecialChar
hi! link lispAtomList SpecialChar
hi! link lispAtomMark Keyword
hi! link lispBarSymbol SpecialChar
hi! link lispFunc Function

hi! link luaFunc Function

call s:hi("markdownBlockquote", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("markdownCode", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("markdownCodeDelimiter", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("markdownFootnote", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("markdownId", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("markdownIdDeclaration", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("markdownH1", s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("markdownLinkText", s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("markdownUrl", s:sg4_gui, "", "NONE", "", "NONE", "")
hi! link markdownBold Bold
hi! link markdownBoldDelimiter Keyword
hi! link markdownFootnoteDefinition markdownFootnote
hi! link markdownH2 markdownH1
hi! link markdownH3 markdownH1
hi! link markdownH4 markdownH1
hi! link markdownH5 markdownH1
hi! link markdownH6 markdownH1
hi! link markdownIdDelimiter Keyword
hi! link markdownItalic Italic
hi! link markdownItalicDelimiter Keyword
hi! link markdownLinkDelimiter Keyword
hi! link markdownLinkTextDelimiter Keyword
hi! link markdownListMarker Keyword
hi! link markdownRule Keyword
hi! link markdownHeadingDelimiter Keyword

call s:hi("perlPackageDecl", s:sg7_gui, "", s:sg7_term, "", "", "")

call s:hi("phpClasses", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("phpClass", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("phpDocTags", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link phpDocCustomTags phpDocTags
hi! link phpMemberSelector Keyword
hi! link phpMethod Function
hi! link phpFunction Function

call s:hi("podCmdText", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("podVerbatimLine", s:sg4_gui, "", "NONE", "", "", "")
hi! link podFormat Keyword

hi! link pythonBuiltin Type
hi! link pythonEscape SpecialChar

call s:hi("rubyConstant", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("rubySymbol", s:sg6_gui, "", s:sg6_term, "", s:bold, "")
hi! link rubyAttribute Identifier
hi! link rubyBlockParameterList Operator
hi! link rubyInterpolationDelimiter Keyword
hi! link rubyKeywordAsMethod Function
hi! link rubyLocalVariableOrMethod Function
hi! link rubyPseudoVariable Keyword
hi! link rubyRegexp SpecialChar

call s:hi("rustAttribute", s:sg10_gui, "", s:sg10_term, "", "", "")
call s:hi("rustEnum", s:sg7_gui, "", s:sg7_term, "", s:bold, "")
call s:hi("rustMacro", s:sg8_gui, "", s:sg8_term, "", s:bold, "")
call s:hi("rustModPath", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("rustPanic", s:sg9_gui, "", s:sg9_term, "", s:bold, "")
call s:hi("rustTrait", s:sg7_gui, "", s:sg7_term, "", s:italic, "")
hi! link rustCommentLineDoc Comment
hi! link rustDerive rustAttribute
hi! link rustEnumVariant rustEnum
hi! link rustEscape SpecialChar
hi! link rustQuestionMark Keyword

call s:hi("sassClass", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("sassId", s:sg7_gui, "", s:sg7_term, "", s:underline, "")
hi! link sassAmpersand Keyword
hi! link sassClassChar Delimiter
hi! link sassControl Keyword
hi! link sassControlLine Keyword
hi! link sassExtend Keyword
hi! link sassFor Keyword
hi! link sassFunctionDecl Keyword
hi! link sassFunctionName Function
hi! link sassidChar sassId
hi! link sassInclude SpecialChar
hi! link sassMixinName Function
hi! link sassMixing SpecialChar
hi! link sassReturn Keyword

hi! link shCmdParenRegion Delimiter
hi! link shCmdSubRegion Delimiter
hi! link shDerefSimple Identifier
hi! link shDerefVar Identifier

hi! link sqlKeyword Keyword
hi! link sqlSpecial Keyword

call s:hi("vimAugroup", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("vimMapRhs", s:sg9_gui, "", s:sg7_term, "", "", "")
call s:hi("vimNotation", s:sg15_gui, "", s:sg7_term, "", "", "")
hi! link vimFunc Function
hi! link vimFunction Function
hi! link vimUserFunc Function

call s:hi("xmlAttrib", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("xmlCdataStart", s:sg3_gui_bright, "", s:sg3_term, "", s:bold, "")
call s:hi("xmlNamespace", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link xmlAttribPunct Delimiter
hi! link xmlCdata Comment
hi! link xmlCdataCdata xmlCdataStart
hi! link xmlCdataEnd xmlCdataStart
hi! link xmlEndTag xmlTagName
hi! link xmlProcessingDelim Keyword
hi! link xmlTagName Keyword

call s:hi("yamlBlockMappingKey", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link yamlBool Keyword
hi! link yamlDocumentStart Keyword

"+----------------+
"+ Plugin Support +
"+----------------+
"+--- UI ---+
" ALE
" > w0rp/ale
call s:hi("ALEWarningSign", s:sg13_gui, "", s:sg13_term, "", "", "")
call s:hi("ALEErrorSign" , s:sg11_gui, "", s:sg11_term, "", "", "")
call s:hi("ALEWarning" , s:sg13_gui, "", s:sg13_term, "", "undercurl", "")
call s:hi("ALEError" , s:sg11_gui, "", s:sg11_term, "", "undercurl", "")

" Coc
" > neoclide/coc
call s:hi("CocWarningHighlight" , s:sg13_gui, "", s:sg13_term, "", "undercurl", "")
call s:hi("CocErrorHighlight" , s:sg11_gui, "", s:sg11_term, "", "undercurl", "")
call s:hi("CocWarningSign", s:sg13_gui, "", s:sg13_term, "", "", "")
call s:hi("CocErrorSign" , s:sg11_gui, "", s:sg11_term, "", "", "")
call s:hi("CocInfoSign" , s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("CocHintSign" , s:sg10_gui, "", s:sg10_term, "", "", "")

" Nvim LSP
" > neovim/nvim-lsp
call s:hi("LSPDiagnosticsWarning", s:sg13_gui, "", s:sg13_term, "", "", "")
call s:hi("LSPDiagnosticsError" , s:sg11_gui, "", s:sg11_term, "", "", "")
call s:hi("LSPDiagnosticsInformation" , s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("LSPDiagnosticsHint" , s:sg10_gui, "", s:sg10_term, "", "", "")

" GitGutter
" > airblade/vim-gitgutter
call s:hi("GitGutterAdd", s:sg14_gui, "", s:sg14_term, "", "", "")
call s:hi("GitGutterChange", s:sg13_gui, "", s:sg13_term, "", "", "")
call s:hi("GitGutterChangeDelete", s:sg11_gui, "", s:sg11_term, "", "", "")
call s:hi("GitGutterDelete", s:sg11_gui, "", s:sg11_term, "", "", "")

" Signify
" > mhinz/vim-signify
call s:hi("SignifySignAdd", s:sg14_gui, "", s:sg14_term, "", "", "")
call s:hi("SignifySignChange", s:sg13_gui, "", s:sg13_term, "", "", "")
call s:hi("SignifySignChangeDelete", s:sg11_gui, "", s:sg11_term, "", "", "")
call s:hi("SignifySignDelete", s:sg11_gui, "", s:sg11_term, "", "", "")

" fugitive.vim
" > tpope/vim-fugitive
call s:hi("gitcommitDiscardedFile", s:sg11_gui, "", s:sg11_term, "", "", "")
call s:hi("gitcommitUntrackedFile", s:sg11_gui, "", s:sg11_term, "", "", "")
call s:hi("gitcommitSelectedFile", s:sg14_gui, "", s:sg14_term, "", "", "")

" Neogit
" > timuntersberger/neogit
call s:hi("NeogitDiffAddHighlight", s:sg14_gui, s:sg16_gui, s:sg11_term, "", "", "")
call s:hi("NeogitDiffDeleteHighlight", s:sg11_gui, s:sg16_gui, s:sg11_term, "", "", "")
call s:hi("NeogitDiffContextHighlight", s:sg5_gui, s:sg16_gui, s:sg11_term, "", "", "")
call s:hi("NeogitHunkHeader", s:sg0_gui, s:sg15_gui, s:sg1_term, "", "", "")
call s:hi("NeogitHunkHeaderHighlight", s:sg0_gui, s:sg15_gui, s:sg11_term, "", "", "")


call s:hi("NeogitNotificationInfo", s:sg14_gui, "", s:sg11_term, "", "", "")
call s:hi("NeogitNotificationWarning", s:sg13_gui, "", s:sg11_term, "", "", "")
call s:hi("NeogitNotificationError", s:sg11_gui, "", s:sg11_term, "", "", "")

" davidhalter/jedi-vim
call s:hi("jediFunction", s:sg4_gui, s:sg3_gui, "", s:sg3_term, "", "")
call s:hi("jediFat", s:sg8_gui, s:sg3_gui, s:sg8_term, s:sg3_term, s:underline.s:bold, "")

" NERDTree
" > scrooloose/nerdtree
call s:hi("NERDTreeExecFile", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link NERDTreeDirSlash Keyword
hi! link NERDTreeHelp Comment

" NvimTree
" > nvim-tree/nvim-tree.lua
call s:hi ("NvimTreeFolderName", s:sg11_gui_dark, "", s:sg10_term, "", "", "")
call s:hi ("NvimTreeEmptyFolderName", s:sg11_gui_dark, "", s:sg10_term, "", "", "")
call s:hi ("NvimTreeOpenedFolderName", s:sg11_gui_dark, "", s:sg10_term, "", "", "")
call s:hi ("NvimTreeSymlinkFolderName", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi ("NvimTreeExecFile", s:sg14_gui, "", s:sg7_term, "", "", "")
call s:hi ("NvimTreeSpecialFile", s:sg12_gui, "", s:sg12_term, "", "", "")
call s:hi ("NvimTreeImageFile", s:sg15_gui, "", s:sg13_term, "", "", "")
call s:hi ("NvimTreeSymlink", s:sg15_gui, "", s:sg12_term, "", "", "")

" CtrlP
" > ctrlpvim/ctrlp.vim
hi! link CtrlPMatch Keyword
hi! link CtrlPBufferHid Normal

" vim-clap
" > liuchengxu/vim-clap
call s:hi("ClapDir", s:sg4_gui, "", "", "", "", "")
call s:hi("ClapDisplay", s:sg4_gui, s:sg1_gui, "", s:sg1_term, "", "")
call s:hi("ClapFile", s:sg4_gui, "", "", "NONE", "", "")
call s:hi("ClapMatches", s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("ClapNoMatchesFound", s:sg13_gui, "", s:sg13_term, "", "", "")
call s:hi("ClapSelected", s:sg7_gui, "", s:sg7_term, "", s:bold, "")
call s:hi("ClapSelectedSign", s:sg9_gui, "", s:sg9_term, "", "", "")

let s:clap_matches = [
        \ [s:sg8_gui,  s:sg8_term] ,
        \ [s:sg9_gui,  s:sg9_term] ,
        \ [s:sg10_gui, s:sg10_term] ,
        \ ]
for s:sg_clap_match_i in range(1,12)
  let clap_match_color = s:clap_matches[s:sg_clap_match_i % len(s:clap_matches) - 1]
  call s:hi("ClapMatches" . s:sg_clap_match_i, clap_match_color[0], "", clap_match_color[1], "", "", "")
  call s:hi("ClapFuzzyMatches" . s:sg_clap_match_i, clap_match_color[0], "", clap_match_color[1], "", "", "")
endfor
unlet s:sg_clap_match_i

hi! link ClapCurrentSelection PmenuSel
hi! link ClapCurrentSelectionSign ClapSelectedSign
hi! link ClapInput Pmenu
hi! link ClapPreview Pmenu
hi! link ClapProviderAbout ClapDisplay
hi! link ClapProviderColon Type
hi! link ClapProviderId Type

" vim-indent-guides
" > nathanaelkane/vim-indent-guides
call s:hi("IndentGuidesEven", "", s:sg1_gui, "", s:sg1_term, "", "")
call s:hi("IndentGuidesOdd", "", s:sg2_gui, "", s:sg3_term, "", "")

" vim-plug
" > junegunn/vim-plug
call s:hi("plugDeleted", s:sg11_gui, "", "", s:sg11_term, "", "")

" vim-signature
" > kshenoy/vim-signature
call s:hi("SignatureMarkText", s:sg8_gui, "", s:sg8_term, "", "", "")

" vim-startify
" > mhinz/vim-startify
call s:hi("StartifyFile", s:sg6_gui, "", s:sg6_term, "", "", "")
call s:hi("StartifyFooter", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("StartifyHeader", s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("StartifyNumber", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("StartifyPath", s:sg8_gui, "", s:sg8_term, "", "", "")
hi! link StartifyBracket Delimiter
hi! link StartifySlash Normal
hi! link StartifySpecial Comment

"+--- Languages ---+
" Haskell
" > neovimhaskell/haskell-vim
call s:hi("haskellPreProc", s:sg10_gui, "", s:sg10_term, "", "", "")
call s:hi("haskellType", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link haskellPragma haskellPreProc

" JavaScript
" > pangloss/vim-javascript
call s:hi("jsGlobalNodeObjects", s:sg8_gui, "", s:sg8_term, "", s:italic, "")
hi! link jsBrackets Delimiter
hi! link jsFuncCall Function
hi! link jsFuncParens Delimiter
hi! link jsThis Keyword
hi! link jsNoise Delimiter
hi! link jsPrototype Keyword
hi! link jsRegexpString SpecialChar

" TypeScript
" > HerringtonDarkholme/yats.vim
call s:hi("typescriptBOMWindowMethod", s:sg8_gui, "", s:sg8_term, "", s:italic, "")
call s:hi("typescriptClassName", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("typescriptDecorator", s:sg12_gui, "", s:sg12_term, "", "", "")
call s:hi("typescriptInterfaceName", s:sg7_gui, "", s:sg7_term, "", s:bold, "")
call s:hi("typescriptRegexpString", s:sg13_gui, "", s:sg13_term, "", "", "")
" TypeScript JSX
 call s:hi("tsxAttrib", s:sg7_gui, "", s:sg7_term, "", "", "")
hi! link typescriptOperator Operator
hi! link typescriptBinaryOp Operator
hi! link typescriptAssign Operator
hi! link typescriptMember Identifier
hi! link typescriptDOMStorageMethod Identifier
hi! link typescriptArrowFuncArg Identifier
hi! link typescriptGlobal typescriptClassName
hi! link typescriptBOMWindowProp Function
hi! link typescriptArrowFuncDef Function
hi! link typescriptAliasDeclaration Function
hi! link typescriptPredefinedType Type
hi! link typescriptTypeReference typescriptClassName
hi! link typescriptTypeAnnotation Structure
hi! link typescriptDocNamedParamType SpecialComment
hi! link typescriptDocNotation Keyword
hi! link typescriptDocTags Keyword
hi! link typescriptImport Keyword
hi! link typescriptExport Keyword
hi! link typescriptTry Keyword
hi! link typescriptVariable Keyword
hi! link typescriptBraces Normal
hi! link typescriptObjectLabel Normal
hi! link typescriptCall Normal
hi! link typescriptClassHeritage typescriptClassName
hi! link typescriptFuncTypeArrow Structure
hi! link typescriptMemberOptionality Structure
hi! link typescriptNodeGlobal typescriptGlobal
hi! link typescriptTypeBrackets Structure
hi! link tsxEqual Operator
hi! link tsxIntrinsicTagName htmlTag
hi! link tsxTagName tsxIntrinsicTagName

" Markdown
call s:hi("mkdCode", s:sg7_gui, "", s:sg7_term, "", "", "")
call s:hi("mkdFootnote", s:sg8_gui, "", s:sg8_term, "", "", "")
call s:hi("mkdRule", s:sg10_gui, "", s:sg10_term, "", "", "")
call s:hi("mkdLineBreak", s:sg9_gui, "", s:sg9_term, "", "", "")
hi! link mkdBold Bold
hi! link mkdItalic Italic
hi! link mkdString Keyword
hi! link mkdCodeStart mkdCode
hi! link mkdCodeEnd mkdCode
hi! link mkdBlockquote Comment
hi! link mkdListItem Keyword
hi! link mkdListItemLine Normal
hi! link mkdFootnotes mkdFootnote
hi! link mkdLink markdownLinkText
hi! link mkdURL markdownUrl
hi! link mkdInlineURL mkdURL
hi! link mkdID Identifier
hi! link mkdLinkDef mkdLink
hi! link mkdLinkDefTarget mkdURL
hi! link mkdLinkTitle mkdInlineURL
hi! link mkdDelimiter Keyword

" Vimwiki
" > vimwiki/vimwiki
if !exists("g:vimwiki_hl_headers") || g:vimwiki_hl_headers == 0
  for s:i in range(1,6)
    call s:hi("VimwikiHeader".s:i, s:sg8_gui, "", s:sg8_term, "", s:bold, "")
  endfor
else
  let s:vimwiki_hcolor_guifg = [s:sg7_gui, s:sg8_gui, s:sg9_gui, s:sg10_gui, s:sg14_gui, s:sg15_gui]
  let s:vimwiki_hcolor_ctermfg = [s:sg7_term, s:sg8_term, s:sg9_term, s:sg10_term, s:sg14_term, s:sg15_term]
  for s:i in range(1,6)
    call s:hi("VimwikiHeader".s:i, s:vimwiki_hcolor_guifg[s:i-1] , "", s:vimwiki_hcolor_ctermfg[s:i-1], "", s:bold, "")
  endfor
endif

call s:hi("VimwikiLink", s:sg8_gui, "", s:sg8_term, "", s:underline, "")
hi! link VimwikiHeaderChar markdownHeadingDelimiter
hi! link VimwikiHR Keyword
hi! link VimwikiList markdownListMarker

" YAML
" > stephpy/vim-yaml
call s:hi("yamlKey", s:sg7_gui, "", s:sg7_term, "", "", "")
